#include "TAnaManager.hxx"
#include "TV1725Waveform.h"
#include "TV1725CumulativeUnfilteredWaveform.h"
#include "TV1725CumulativeFilteredWaveform.h"
#include "TScientificHistograms.h"
#include "TWaveformProcessor.hxx"
#include "TChannelHistograms.h"
#include "TPulseHistograms.h"
#include "TChronoHistograms.h"
#include "TPulseInjector.hxx"

TAnaManager::TAnaManager(){

  //AddHistogram(new TV1725CumulativeUnfilteredWaveform());
  //AddHistogram(new TV1725CumulativeFilteredWaveform());
  AddHistogram(new TV1725Waveform());
  //AddHistogram(new TChannelHistograms());
  //AddHistogram(new TBaselineSummary());
  //AddHistogram(new TChannelRMS());
  //AddHistogram(new TRMSSummary());
  AddHistogram(new TPulseHeights());
  AddHistogram(new TPulseCharges());
  AddHistogram(new TTotalWaveform());
  AddHistogram(new TTotalTimes());
  AddHistogram(new TF90VsNPE);
  AddHistogram(new TNPESpectrum);
  AddHistogram(new TPulsesVsTime());
  //AddHistogram(new TTriggerType());
  //AddHistogram(new TTriggerPrimitives());
};


void TAnaManager::AddHistogram(THistogramArrayBase* histo) {
  histo->DisableAutoUpdate();

  //histo->CreateHistograms();
  fHistos.push_back(histo);

}


int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){
  // Instantiate and define parameters of Eulero, the class describing the SiPM pulses
  dswp::instance()->SetEuler();
  // Instantiate a TPInjector and define parameters of fake pulses to be added to the empty waveforms
  TPInjector tpi;
  tpi.ImportEuler(dswp::instance()->GetEuler());
  tpi.set_A(100);
  tpi.set_sigmaA(5);
  tpi.set_mu(2);
  tpi.set_pretrigger(0.1);
  tpi.set_jitter(3);

  // Process the waveforms, calculating relevant quantities.
  // Quantities will be put in histograms in next stage.
  dswp::instance()->ProcessWaveforms(dataContainer);

  // Inject pulses in the unfiltered waveforms
  dswp::instance()->InjectPulses(tpi,0);

  // Filter waveforms acquired
  dswp::instance()->FilterWaveforms();

  // Find the pulses
  dswp::instance()->FindPulses();

  // Fill all the  histograms
  for (unsigned int i = 0; i < fHistos.size(); i++) {
    // Some histograms are very time-consuming to draw, so we can
    // delay their update until the canvas is actually drawn.
    if (!fHistos[i]->IsUpdateWhenPlotted()) {
      fHistos[i]->UpdateHistograms(dataContainer);
    }
  }
  
  return 1;
}


// Little trick; we only fill the transient histograms here (not cumulative), since we don't want
// to fill histograms for events that we are not displaying.
// It is pretty slow to fill histograms for each event.
void TAnaManager::UpdateTransientPlots(TDataContainer& dataContainer){
  std::vector<THistogramArrayBase*> histos = GetHistograms();
  
  for (unsigned int i = 0; i < histos.size(); i++) {
    if (histos[i]->IsUpdateWhenPlotted()) {
      histos[i]->UpdateHistograms(dataContainer);
    }
  }
}


