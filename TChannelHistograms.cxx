#include "TChannelHistograms.h"

#include "TDirectory.h"
#include "TH2D.h"
#include "TWaveformProcessor.hxx"


const int numberChannelPerModule = 16;
/// Reset the histograms for this canvas
TChannelHistograms::TChannelHistograms(){

  SetTabName("Baselines");
  SetSubTabName("Channel Baselines");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}


void TChannelHistograms::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"Baselines_0_0");
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }
   
  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"Baselines_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
      
      sprintf(title,"Waveform Baselines module=%i, channel=%i",j,i);	
      // adapt to 15000 DAC setting (MR 7/12/2019)      
      //      TH1D *tmp = new TH1D(name, title, 2000, 14500, 15500);
      TH1D *tmp = new TH1D(name, title, 2000, 14000, 16000);
      tmp->SetXTitle("ADC value");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}


void TChannelHistograms::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();
  for(int i = 0; i < chans.size(); i++){
    TDSChannel chan = chans[i];
    int index = chan.module*16 + chan.channel;
    GetHistogram(index)->Fill(chan.baseline);
    dswp::instance()->GetChannels()[i].baseline_mean = GetHistogram(index)->GetMean();
  }
  
}




TBaselineSummary::TBaselineSummary(){

  SetTabName("Baselines");
  SetSubTabName("Baseline Summary");
  CreateHistograms();
}


void TBaselineSummary::CreateHistograms(){

  // check if we already have histogramss.
  TH2D *tmp = (TH2D*)gDirectory->Get("Baseline_Summary");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH2D("Baseline_Summary", "Baseline Summary", 16, -0.5, 15.5, 4,-0.5,3.5);  
  tmp->SetXTitle("Channel Number");
  tmp->SetYTitle("Module Number");
  
  push_back(tmp);

}


void TBaselineSummary::UpdateHistograms(TDataContainer& dataContainer){

    // Grab the data from the singleton
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();
  for(int i = 0; i < chans.size(); i++){
    TDSChannel chan = chans[i];
    GetHistogram(0)->SetBinContent(chan.channel+1,chan.module+1,chan.baseline_mean);
  }  
}



// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TBaselineSummary::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Baseline Summary", "COLZ");    
}




TChannelRMS::TChannelRMS(){

  SetTabName("Baselines");
  SetSubTabName("Baseline RMS");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}


void TChannelRMS::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"BaselinesRMS_0_0");    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"BaselinesRMS_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
      
      sprintf(title,"Baseline RMS module=%i, channel=%i",j,i);	
      
      // very noisy RMS=100 ADC
      //      TH1D *tmp = new TH1D(name, title, 1000, 0, 50);
      TH1D *tmp = new TH1D(name, title, 2000, 0, 500);
      tmp->SetXTitle("ADC value");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}


void TChannelRMS::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();
  for(int i = 0; i < chans.size(); i++){
    TDSChannel chan = chans[i];
    int index = chan.module*16 + chan.channel;
    GetHistogram(index)->Fill(chan.baseline_rms);
    dswp::instance()->GetChannels()[i].baseline_rms_mean = GetHistogram(index)->GetMean();
  }
  
}




TRMSSummary::TRMSSummary(){

  SetTabName("Baselines");
  SetSubTabName("Baseline RMS Summary");
  CreateHistograms();
}


void TRMSSummary::CreateHistograms(){

  // check if we already have histogramss.

  TH2D *tmp = (TH2D*)gDirectory->Get("Baseline_RMS_Summary");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH2D("Baseline_RMS_Summary", "Baseline RMS Summary", 16, -0.5, 15.5, 4,-0.5,3.5);  
  tmp->SetXTitle("Channel Number");
  tmp->SetYTitle("Module Number");
  
  push_back(tmp);

}


void TRMSSummary::UpdateHistograms(TDataContainer& dataContainer){

    // Grab the data from the singleton
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();
  for(int i = 0; i < chans.size(); i++){
    TDSChannel chan = chans[i];
    GetHistogram(0)->SetBinContent(chan.channel+1,chan.module+1,chan.baseline_rms_mean);
  }  
}



// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TRMSSummary::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Baseline RMS Summary", "COLZ");    
}
