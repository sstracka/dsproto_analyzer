#include "TChronoData.h"

#include <iomanip>
#include <iostream>


TChronoData::TChronoData(int bklen, int bktype, const char* name, void *pdata):
    TGenericData(bklen, bktype, name, pdata)
{

  if(bklen < 10) { std::cerr <<"Errorr in  TChronoData; bklen < 10" <<std::endl;return;}
  
  Counter = GetData32()[0];
  AcceptedTriggers = GetData32()[1]; 
  DroppedTriggers = GetData32()[2]; 
  Timestamp = (uint64_t)GetData32()[3] + (((uint64_t)(GetData32()[4])) << 32);
  TriggerReason = ((GetData32()[5] >> 16) & 0xffff);
  TriggerType = GetData32()[5] & 0xffff;
  ChannelEnable = GetData32()[6];
  ChannelTriggerPattern = GetData32()[7];
  ChannelAssignment = GetData32()[8]; 
  
}

void TChronoData::Print(){
  std::cout << "Chronobox data: ";

  std::cout << std::hex 
	    << Counter << " " << AcceptedTriggers << " " << DroppedTriggers << " "
            << Timestamp << " " << TriggerReason << " " << TriggerType << " "
            << ChannelTriggerPattern << " " << GetData32()[9] << std::dec 
	    << std::endl;
  
}
