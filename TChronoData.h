#ifndef TChronoData_hxx_seen
#define TChronoData_hxx_seen

#include <vector>

#include "TGenericData.hxx"



/// Class to store Darkside Chronobox
class TChronoData: public TGenericData {

public:

  /// Constructor
  TChronoData(int bklen, int bktype, const char* name, void *pdata);

  void Print();

  uint32_t Counter; // counter of number of transmitted events
  uint32_t AcceptedTriggers; // Number of Accepted Triggers
  uint32_t DroppedTriggers; // Number of dropped triggers
  uint64_t Timestamp;
  uint32_t TriggerReason;
  uint32_t TriggerType;
  uint32_t ChannelEnable;
  uint32_t ChannelTriggerPattern; // Which trigger primitives partiticipanted in this event
  uint32_t ChannelAssignment; // Mask of which trigger primitive channels are assigned to top/bottom
  
  

private:


};

#endif
