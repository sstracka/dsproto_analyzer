#include "TChronoHistograms.h"
#include "TChronoData.h"

#include "TDirectory.h"
#include "TH2D.h"


TTriggerType::TTriggerType(){

  SetTabName("ChronoBox");
  SetSubTabName("Trigger Type");
  CreateHistograms();

}


void TTriggerType::CreateHistograms(){

 // check if we already have histogramss.

  TH1D *tmp = (TH1D*)gDirectory->Get("TriggerType");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH1D("TriggerType", "Trigger Type", 5,-0.5,4.5);  
  tmp->SetXTitle("Trigger Type");
  
  push_back(tmp);

}


void TTriggerType::UpdateHistograms(TDataContainer& dataContainer){

  TChronoData *chrono = dataContainer.GetEventData<TChronoData>("ZMQ0");
  if(chrono){
    GetHistogram(0)->Fill(chrono->TriggerReason);
    chrono->Print();
  }
  
}


// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTriggerType::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Trigger Type");    
}



TTriggerPrimitives::TTriggerPrimitives(){

  SetTabName("ChronoBox");
  SetSubTabName("Trigger Primitives");
  CreateHistograms();

}


void TTriggerPrimitives::CreateHistograms(){

 // check if we already have histogramss.

  TH2D *tmp = (TH2D*)gDirectory->Get("TriggerPrimitives");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH2D("TriggerPrimitives", "Trigger Primitives Counts vs Chan Group and Module", 8,-0.5,7.5,4,-0.5,3.5);  
  tmp->SetXTitle("Channel Group");
  tmp->SetYTitle("V1725 Module");
  
  push_back(tmp);

}


void TTriggerPrimitives::UpdateHistograms(TDataContainer& dataContainer){

  TChronoData *chrono = dataContainer.GetEventData<TChronoData>("ZMQ0");
  if(chrono){
    for(int i = 0; i < 8; i++){ // loop over groups
      for(int j = 0; j < 4; j++){
	int shift = i + j*8;
	int bit = 1 << shift;
	bool test = chrono->ChannelTriggerPattern & bit;
	//std::cout << std::hex << bit << " " << chrono->ChannelTriggerPattern << std::dec 
	//	  << " " << test<< std::endl;
	if(test){
	  GetHistogram(0)->Fill(i,j);
	}
      }
    }
  }
  
}


// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTriggerPrimitives::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Trigger Primitives","COLZ");    
}
