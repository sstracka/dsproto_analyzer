#ifndef TChronoHistograms_h
#define TChronoHistograms_h

#include <string>
#include "THistogramArrayBase.h"
#include "TSimpleHistogramCanvas.hxx"


// Check track of trigger Type
class TTriggerType : public THistogramArrayBase {
public:
  TTriggerType();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};

// Check track of trigger primitives
class TTriggerPrimitives : public THistogramArrayBase {
public:
  TTriggerPrimitives();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};


#endif


