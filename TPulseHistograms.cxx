#include "TPulseHistograms.h"

#include "TDirectory.h"
#include "TH2D.h"
#include "TWaveformProcessor.hxx"


const int numberChannelPerModule = 16;
TPulseCharges::TPulseCharges(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Charge (roi)");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}


void TPulseCharges::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"PulseCharge_0_0");
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"PulseCharge_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
 
      sprintf(title,"Pulse Charge (roi) module=%i, channel=%i",j,i);	
      
      TH1D *tmp = new TH1D(name, title, 5000, 0,500000);
      tmp->SetXTitle("Pulse Charge (ADC*sample value)");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}


void TPulseCharges::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(pulse.index)->Fill(pulse.charge);
  }
  
}

TPulseHeights::TPulseHeights(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Heights");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}


void TPulseHeights::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"PulseHeight_0_0");
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"PulseHeight_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
 
      sprintf(title,"Pulse Height module=%i, channel=%i",j,i);	
      
      TH1D *tmp = new TH1D(name, title, 2000, 0,500);
      tmp->SetXTitle("Pulse Height (ADC value)");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}


void TPulseHeights::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  bool okindex[48]={48*false};
  for(int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    if (okindex[pulse.index]==false) {
      okindex[pulse.index]=true;
      GetHistogram(pulse.index)->Fill(pulse.height);
    }
  }
  
}

TPulsesVsTime::TPulsesVsTime(){

  SetTabName("Pulses");
  SetSubTabName("Pulses Vs Time");
  CreateHistograms();

}


void TPulsesVsTime::CreateHistograms(){

 // check if we already have histogramss.

  TH2D *tmp = (TH2D*)gDirectory->Get("Pulses_Vs_Time");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH2D("Pulses_Vs_Time", "Number of Pulses Vs Time", 500,0,5000,64,-0.5,63.5);  
  tmp->SetXTitle("Pulse Time (ns)");
  tmp->SetYTitle("Channel index");
  
  push_back(tmp);

}


void TPulsesVsTime::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(0)->Fill(pulse.time,pulse.index);
  }
  
}


// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TPulsesVsTime::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Pulses Vs Time", "COLZ");    
}
