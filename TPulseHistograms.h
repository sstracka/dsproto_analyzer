#ifndef TPulseHistograms_h
#define TPulseHistograms_h

#include <string>
#include "THistogramArrayBase.h"
#include "TSimpleHistogramCanvas.hxx"

/// Class for making histograms of waveform baselines
class TPulseHeights : public THistogramArrayBase {
public:
  TPulseHeights();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};

class TPulseCharges : public THistogramArrayBase {
public:
  TPulseCharges();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};


class TPulsesVsTime : public THistogramArrayBase {
public:
  TPulsesVsTime();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};



#endif


