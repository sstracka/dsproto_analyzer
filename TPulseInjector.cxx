#include "TPulseInjector.hxx"
#include "eulero.hxx"
#include <algorithm>

using namespace std;

TPInjector::TPInjector(){
  A = 100.;
  sigmaA = 10.;
  mu = 1;
  pretrigger = 0.5;
  jitter = 0;
  r.SetSeed(0);
}

void TPInjector::ImportEuler(eulero& eu0){
  eu.set_tau(eu0.get_tau());
  eu.set_sigma_fp(eu0.get_sigma_fp());
  eu.set_Afp_Aexp_ratio(eu0.get_Afp_Aexp_ratio());
  eu.set_fast_exp_charge_ratio(eu0.get_fast_exp_charge_ratio());
  eu.get_MA_gaus_ker().clear();
  eu.set_MA_gaus_ker(eu0.get_MA_gaus_ker());
  eu.get_ARMA_ker().clear();
  eu.set_ARMA_ker(eu0.get_ARMA_ker());
  return; 
}

void TPInjector::InjectPulse(std::vector<double> &wf, int sample0, float NPE){
  int i, j;
  vector<double> ker = eu.get_ARMA_ker();
  double ampl = 0; 
  for(int n = 0; n < NPE; n++) ampl += r.Gaus(A,sigmaA);
  if( sample0 >= wf.size()) return;
  else if( sample0 + ker.size() >= wf.size()){
    j = 0;
    for(i = sample0; i < wf.size(); i++){
      wf[i] += ampl*ker[j];
      j++;
    }
  }
  else {
    j = 0;
    for(i = sample0; i < sample0 + ker.size(); i++){
      wf[i] += ampl*ker[j];
      j++;
    }
  }
  return;
}

void TPInjector::InjectPoissonNPE(std::vector<double> &wf){
  int sample0 = int(round(pretrigger*wf.size()) + round(r.Gaus(0,jitter)));
  int NPE = r.Poisson(mu);
  InjectPulse(wf,sample0,NPE);
  return;
}

