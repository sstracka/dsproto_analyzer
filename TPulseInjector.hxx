#ifndef TPulseInjector_h
#define TPulseInjector_h

#include <vector>
#include <cmath>
#include <TRandom3.h>
#include "eulero.hxx"

/// Class to store a pulse
class TPInjector{

public:
  TPInjector();
  eulero eu;
  void ImportEuler( eulero& eu0);
  void InjectPulse(std::vector<double> &wf, int sample0, float NPE);
  void InjectPoissonNPE(std::vector<double> &wf);

  void set_A(double A0){ A = A0;};
  double get_A() { return A; }
  void set_sigmaA(double sigmaA0){ sigmaA = sigmaA0;};
  double get_sigmaA() { return sigmaA; }
  void set_mu(double mu0){ mu = mu0;};
  double get_mu() { return mu; }
  void set_pretrigger(double pretrigger0){ pretrigger = pretrigger0;};
  double get_pretrigger() { return pretrigger; }
  void set_jitter(double jitter0){ jitter = jitter0;};
  double get_jitter() { return jitter; }

private:
  // template amplitude + variance
  double A;
  double sigmaA;
  // injected pulses paramters
  double mu;
  double pretrigger; // [0-1]
  double jitter; // in samples
  // random number generator
  TRandom3 r;
};

#endif
