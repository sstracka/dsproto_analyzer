#include "TScientificHistograms.h"

#include "TSimpleHistogramCanvas.hxx"
#include "TWaveformProcessor.hxx"
#include "TDirectory.h"
#include "TH2F.h"
#include <iostream> 
#include <cmath>

using namespace std;

const int numberChannelPerModule = 16;

/// Reset the histograms for this canvas
TTotalWaveform::TTotalWaveform(){

  SetSubTabName("V1725 Total Unfiltered Waveforms");
  SetUpdateOnlyWhenPlotted(false);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TTotalWaveform::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TUW");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TUW");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();	
      
  TH1D *tmp = new TH1D("V1725_TUW", "V1725 Total Unfiltered Waveform", fNSamples, 0, WFLength);
  tmp->SetXTitle("ns");
  tmp->SetYTitle("ADC value");

  push_back(tmp);
}


void TTotalWaveform::UpdateHistograms(TDataContainer& dataContainer){

  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() != 0){
    if(chans[0].unfiltered_wf.size() != GetHistogram(0)->GetNbinsX()){
      fNSamples = chans[0].filtered_wf.size();
      CreateHistograms(true);
      std::cout << "V1725Waveforms: updating the filtered waveform size to " << fNSamples 
	        << " bins." << std::endl;
    }
  }
  else return;
  
  double wf[fNSamples];
  for(int i = 0; i < fNSamples; i++) wf[i] = 0 ;
  for(int i = 0; i < chans.size(); i++){
    for(int j = 0; j < chans[i].unfiltered_wf.size(); j++){   
      wf[j] += chans[i].unfiltered_wf[j];
    }
  }
  // Fill the histogram
  for(int j = 0; j < chans[0].unfiltered_wf.size(); j++){
    GetHistogram(0)->SetBinContent((j+1),wf[j]); 
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalWaveform::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Total Unfiltered Waveform", "COLZ");    
}


void TTotalWaveform::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}



/// TTotalTimes class: plots the times of all found pulses
/// Reset the histograms for this canvas
TTotalTimes::TTotalTimes(){

  SetSubTabName("Arrival Times of all found pulses");
  SetUpdateOnlyWhenPlotted(false);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TTotalTimes::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("TTT");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("TTT");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();	
      
  TH1D *tmp = new TH1D("TTT", "Arrival Times of all found pulses", fNSamples, 0, WFLength);
  tmp->SetXTitle("ns");
  tmp->SetYTitle("Pulse Height");

  push_back(tmp);
}


void TTotalTimes::UpdateHistograms(TDataContainer& dataContainer){

  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() != 0){
    if(chans[0].filtered_wf.size() != GetHistogram(0)->GetNbinsX()){
      fNSamples = chans[0].filtered_wf.size();
      CreateHistograms(true);
    }
  }
  else return;

  Reset();
  // Fill the histogram
  for(int j = 0; j < pulses.size(); j++){
    GetHistogram(0)->AddBinContent(int(round(pulses[j].time/nanosecsPerSample)),pulses[j].height); 
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalTimes::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Arrival Times of all found pulses", "COLZ");    
}


void TTotalTimes::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}


/// TF90VsNPE class: plots the F90 vs NPE of all events
/// Reset the histograms for this canvas
TF90VsNPE::TF90VsNPE(){

  SetSubTabName("F90 vs NPE");
  SetUpdateOnlyWhenPlotted(false);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TF90VsNPE::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH2D *tmp = (TH2D*)gDirectory->Get("F90vsNPE");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH2D *tmp = (TH2D*)gDirectory->Get("F90vsNPE");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();	
      
  TH2D *tmp = new TH2D("F90vsNPE", "F90 Vs NPE", 500, 0, 25000,100,0,1);
  tmp->SetXTitle("NPE");
  tmp->SetYTitle("F90");

  push_back(tmp);
}


void TF90VsNPE::UpdateHistograms(TDataContainer& dataContainer){

  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
 
  // Fill the histogram
  double fast_height = 0;
  double tot_height = 0;
  double t0 = 0;
  if(pulses.size() == 0) return;
  sort(pulses.begin(), pulses.end(), [](TDSPulse p1, TDSPulse p2) {return p1.time < p2.time;});
  t0 = pulses[0].time;
  for(int j = 0; j < pulses.size(); j++){
    if(pulses[j].time - t0 <= 90) fast_height += pulses[j].height;
    if(pulses[j].time - t0 < 30000) tot_height += pulses[j].height; // patch to ignore the REAL pulses currently registered in run635
 }
  GetHistogram(0)->Fill(tot_height,fast_height/tot_height); 
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TF90VsNPE::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"F90 Vs NPE", "COLZ");    
}


void TF90VsNPE::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}


/// TNPESpectrum class: plots the NPE spectrum of all events
/// Reset the histograms for this canvas
TNPESpectrum::TNPESpectrum(){

  SetSubTabName("NPE Spectrum");
  SetUpdateOnlyWhenPlotted(false);

  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TNPESpectrum::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("NPESpectrum");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("NPESpectrum");
    if (tmp) return;
  }
  // Otherwise make histograms
  clear();	
      
  TH1D *tmp = new TH1D("NPESpectrum", "NPE Spectrum", 500, 0, 25000);
  tmp->SetXTitle("NPE");
  tmp->SetYTitle("Events");

  push_back(tmp);
}


void TNPESpectrum::UpdateHistograms(TDataContainer& dataContainer){

  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
 
  // Fill the histogram
  double tot_height = 0;
  if(pulses.size() == 0) return;
  for(int j = 0; j < pulses.size(); j++){
    if(pulses[j].time < 110000) tot_height += pulses[j].height; // patch to ignore the REAL pulses currently registered in run635
 }
  GetHistogram(0)->Fill(tot_height); 
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TNPESpectrum::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"NPE Spectrum", "COLZ");    
}


void TNPESpectrum::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}
