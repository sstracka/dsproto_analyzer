#ifndef TScientificHistograms_h
#define TScientificHistograms_h

#include <string>
#include "THistogramArrayBase.h"

/// Class for making a total waveform histograms;
class TTotalWaveform : public THistogramArrayBase {
public:
  TTotalWaveform();
  virtual ~TTotalWaveform(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
 
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
 
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){               
    CreateHistograms();         
  }
  
private:
  int nanosecsPerSample;
  int fNSamples;
};


class TTotalTimes : public THistogramArrayBase {
public:
  TTotalTimes();
  virtual ~TTotalTimes(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
 
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
 
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){               
    CreateHistograms();         
  }
  
private:
  int nanosecsPerSample;
  int fNSamples;
};

class TF90VsNPE : public THistogramArrayBase {
public:
  TF90VsNPE();
  virtual ~TF90VsNPE(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
 
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
 
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){               
    CreateHistograms();         
  }
  
private:
  int nanosecsPerSample;
  int fNSamples;
};

class TNPESpectrum : public THistogramArrayBase {
public:
  TNPESpectrum();
  virtual ~TNPESpectrum(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
 
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
 
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){               
    CreateHistograms();         
  }
};



#endif


