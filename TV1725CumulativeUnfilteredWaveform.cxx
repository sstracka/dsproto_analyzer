#include "TV1725CumulativeUnfilteredWaveform.h"

#include "TWaveformProcessor.hxx"
#include "TDirectory.h"
#include "TH2F.h"
#include <iostream> 
#include <cmath>

using namespace std;

const int numberChannelPerModule = 16;

/// Reset the histograms for this canvas
TV1725CumulativeUnfilteredWaveform::TV1725CumulativeUnfilteredWaveform(){

  SetSubTabName("V1725 Cumulative Unfiltered Waveforms");
  SetUpdateOnlyWhenPlotted(false);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TV1725CumulativeUnfilteredWaveform::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    for(int j = 0; j < 4; j++){ // loop over modules
      for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
	char tname[100];
	sprintf(tname,"V1725_CUW_%i_%i",j,i);	
	TH2D *tmp = (TH2D*)gDirectory->Get(tname);
	delete tmp; 	
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"V1725_CUW_%i_%i",0,0);
    
    TH2F *tmp = (TH2F*)gDirectory->Get(tname);
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"V1725_CUW_%i_%i",j,i);
      TH2F *otmp = (TH2F*)gDirectory->Get(name);
      if (otmp) delete otmp;      
 

      sprintf(title,"V1725 Cumulative Unfiltered Waveform for module=%i, channel=%i",j,i);	
      
      TH2F *tmp = new TH2F(name, title, fNSamples, 0, WFLength, 500, -100, 400);
      tmp->SetXTitle("ns");
      tmp->SetYTitle("ADC value");

      push_back(tmp);
    }
  }
}


void TV1725CumulativeUnfilteredWaveform::UpdateHistograms(TDataContainer& dataContainer){

  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();

  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();
  for(int i = 0; i < chans.size(); i++){
    TDSChannel chan = chans[i];
    int index = chan.module*16 + chan.channel;

    if(i == 0){
      if(chan.unfiltered_wf.size() != GetHistogram(index)->GetNbinsX()){
	fNSamples = chan.unfiltered_wf.size();
        CreateHistograms(true);
	std::cout << "V1725Waveforms: updating the waveform size to " << fNSamples 
	          << " bins." << std::endl;
      }
    }
	
    // Fill the histogram
    float bin_width = GetHistogram(index)->GetXaxis()->GetBinWidth(1); 
    for(int j = 0; j < chan.unfiltered_wf.size(); j++){
      GetHistogram(index)->Fill((j+1)*bin_width,chan.unfiltered_wf[j]); 
    }            		
  }
}



void TV1725CumulativeUnfilteredWaveform::Reset(){

  
  for(int j = 0 ; j < 4; j++){// loop over modules

    for(int i = 0; i < numberChannelPerModule; i++){ // loop over channels
      int index =  i+ j*numberChannelPerModule;;;
      
      // Reset the histogram...
      //for(int ib = 0; ib < GetHistogram(index)->GetNbinsX(); ib++) {
      //  GetHistogram(index)->SetBinContent(ib, 0);
      //}
      
      GetHistogram(index)->Reset();
      
    }
  }
}
