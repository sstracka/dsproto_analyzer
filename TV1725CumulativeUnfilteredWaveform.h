#ifndef TV1725CumulativeUnfilteredWaveform_h
#define TV1725CumulativeUnfilteredWaveform_h

#include <string>
#include "THistogramArrayBase.h"
#include "TSimpleHistogramCanvas.hxx"

/// Class for making histograms of raw V1725 waveforms;
/// right now is only for raw or ZLE compressed data.
class TV1725CumulativeUnfilteredWaveform : public THistogramArrayBase {
public:
  TV1725CumulativeUnfilteredWaveform();
  virtual ~TV1725CumulativeUnfilteredWaveform(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
  
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){		
    CreateHistograms();		
  }
  
private:
  int nanosecsPerSample;
  int fNSamples;
};

#endif


