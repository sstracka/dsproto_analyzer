#include "TV1725RawData.h"

#include <iomanip>
#include <iostream>

void TV1725RawData::HandlZLECompressedData(){

  // >>> Loop over ZLE data and fill up  

  uint32_t chMask1     = GetData32()[1] & 0xFF;
  uint32_t chMask2     = ((GetData32()[2] & 0xFF000000) >> 24);
  uint32_t chMask = chMask1 +  (chMask2 << 8);
  uint32_t iPtr=4;
  for(int iCh=0; iCh<16; iCh++){
    if (chMask & (1<<iCh)){
      uint32_t chSize = GetData32()[iPtr] & 0xffff;
      uint32_t baseline = (GetData32()[iPtr] & 0x3fff0000) >> 16;
      //if(iCh==2) printf("Ch %i, Channel size: %i baseline %i\n",iCh,chSize,baseline);
      uint32_t iChPtr = 1;// The space for first word is included in chSize
      uint32_t iBin=0; // which bin are we at?
      iPtr++;
      
      TV1725RawChannel channel(iCh, IsZLECompressed(),baseline);
      uint32_t total_samples = 0;
      std::vector<uint32_t> samples;      
      while(iChPtr<chSize){

        //printf("data: %x %i %i\n", GetData32()[iPtr], iChPtr,chSize, iBin);
        
        if((GetData32()[iPtr]>>31) & 0x1){ // skipped word
          iBin += ((GetData32()[iPtr]) & 0xfffffff)*2;

          // If iPtr is > 4 then we should have some samples now
          if(iChPtr > 1){
            if(0)std::cout << "Adding ZLE pulse with size: "
                      << samples.size() << " and iBin "
                      << iBin << " and baseline " 
		      << baseline << std::endl;
            // So we create the ZLE pulse
            channel.AddZlePulse(TV1725RawZlePulse(iBin-samples.size(), samples));
            samples.clear();
            
          }
        }else{
          
          samples.push_back((GetData32()[iPtr]&0x3FFF));
          samples.push_back(((GetData32()[iPtr]>>16)&0x3FFF));
          //std::cout << (GetData32()[iPtr]&0x3FFF)
	  //        << " " << ((GetData32()[iPtr]>>16)&0x3FFF) << std::endl;
          iBin += 2;          
        }
	iPtr++;	
	iChPtr++;	
      }

      // Add a ZLE pulse if there is one at the end of the waveform.
      if(samples.size() > 0){
        if(0)std::cout << "Adding final ZLE pulse with size: "
                  << samples.size() << " and iBin "
                  << iBin << std::endl;
        // So we create the ZLE pulse
        channel.AddZlePulse(TV1725RawZlePulse(iBin-samples.size(), samples));
      }
      
      fMeasurements.push_back(channel);
    }
  }

}

void TV1725RawData::HandlUncompressedData(){


  
  // Skip the header.
  uint32_t iPtr=4;
  //  uint32_t chMask    = GetData32()[1] & 0xFF;
  uint32_t chMask1     = GetData32()[1] & 0xFF;
  uint32_t chMask2     = ((GetData32()[2] & 0xFF000000) >> 24);
  uint32_t chMask = chMask1 +  (chMask2 << 8);

  int nActiveChannels=0;
  for(int iCh=0; iCh<16; iCh++){
    if(chMask & (1<<iCh))
      nActiveChannels++;
  }
  // Assume that we have readout the same number of samples for each channel.
  // The number of 32 bit double-samples per channel is then
  // N32samples = (bank size - 4)/ nActiveChannels 
  int N32samples = (GetEventSize() - 4)/ nActiveChannels;

  // Loop over channels
  for(int iCh=0; iCh<16; iCh++){
        
    if(!(chMask & (1<<iCh))){
      // Add empty channel data objects, to keep vector simple.
      TV1725RawChannel channel(iCh, IsZLECompressed());
      fMeasurements.push_back(channel);
      continue;
    }

    TV1725RawChannel channel(iCh, IsZLECompressed());
      
    for(int j = 0; j < N32samples; j++){
      uint32_t samp1 = (GetData32()[iPtr]&0x3FFF);
      uint32_t samp2 = ((GetData32()[iPtr]>>16)&0x3FFF);
      
      channel.AddADCSample(samp1);
      channel.AddADCSample(samp2);      
      iPtr++;
    }

    fMeasurements.push_back(channel);

  }  
}


TV1725RawData::TV1725RawData(int bklen, int bktype, const char* name, void *pdata):
    TGenericData(bklen, bktype, name, pdata)
{

  
  fGlobalHeader0 = GetData32()[0];
  fGlobalHeader1 = GetData32()[1];
  fGlobalHeader2 = GetData32()[2];
  fGlobalHeader3 = GetData32()[3];
  

  if( IsZLECompressed()) std::cout << "Error; data is incorrectly set as being zle compressed..." << std::endl;
  //  if(IsZLECompressed()){ // assume ZLE data for now... need to check format later...
    
  //HandlZLECompressedData();
  //}else{
  HandlUncompressedData();
    //}

}

void TV1725RawData::Print(){

  std::cout << "V1725 decoder for bank " << GetName().c_str() << std::endl;
  std::cout << "Bank size: " << GetEventSize() << std::endl;
  
  if( IsZLECompressed())
    std::cout << "Data is ZLE compressed." << std::endl;
  else
    std::cout << "Data is not ZLE compressed." << std::endl;

  std::cout << "Channel Mask : " << GetChannelMask() << std::endl;
   
  std::cout << "Event counter : " << GetEventCounter() << std::endl;
  std::cout << "Trigger tag: " << GetTriggerTag() << std::endl;
  

  std::cout << "Number of channels with data: " << GetNChannels() << std::endl;
  for(int i = 0 ; i < GetNChannels(); i++){

    TV1725RawChannel channelData = GetChannelData(i);

    std::cout << "Channel: " << channelData.GetChannelNumber() << std::endl;
    
    if(IsZLECompressed()){
      std::cout << "Number of ZLE pulses: " <<  channelData.GetNZlePulses() << std::endl;
      for(int j = 0; j < channelData.GetNZlePulses(); j++){
	std::cout << "Pulse: " << j << std::endl;
	TV1725RawZlePulse pulse = channelData.GetZlePulse(j);	
	std::cout << "first sample bin: " << pulse.GetFirstBin() << std::endl;
	std::cout << "Samples ("<< pulse.GetNSamples()<<  " total): " <<std::endl;
	for(int k = 0; k < pulse.GetNSamples(); k++){
	  std::cout << pulse.GetSample(k) << ", ";
	  if(k%12 == 11) std::cout << std::endl;
	}
	std::cout << std::endl;
	
      }
    }

  }

}
