#ifndef TV1725Waveform_h
#define TV1725Waveform_h

#include <string>
#include "THistogramArrayBase.h"

/// Class for making histograms of raw V1725 waveforms;
/// right now is only for raw or ZLE compressed data.
class TV1725Waveform : public THistogramArrayBase {
public:
  TV1725Waveform();
  virtual ~TV1725Waveform(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();
  
  void CreateHistograms(bool force = false);
  
  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){		
    CreateHistograms();		
  }
  
private:
  int nanosecsPerSample;
  int fNSamples;
};

#endif


