#ifndef EULERO_HH
#define EULERO_HH

#include <cmath>
#include <vector>

using namespace std;

class eulero {
  public:
    eulero();
    eulero( eulero &eu0 );
    void set_tau(double tau0) { tau = tau0; };
    double get_tau() { return tau; }
    void set_Afp_Aexp_ratio(double r) { Afp_Aexp_ratio = r; };
    double get_Afp_Aexp_ratio() { return Afp_Aexp_ratio; }
    void set_sigma_fp(double sigma_fp0) { sigma_fp = sigma_fp0; };
    double get_sigma_fp() { return sigma_fp; }
    void set_fast_exp_charge_ratio(double fast_exp_ratio0) { fast_exp_ratio = fast_exp_ratio0; }
    double get_fast_exp_charge_ratio() { return fast_exp_ratio; }
 
   void calc_fast_exp_ratio();
   void gen_MA_gaus_ker();
   void gen_ARMA_ker();
   void set_MA_gaus_ker(vector<double> gaus_ker0){ MA_gaus_ker.assign(gaus_ker0.begin(),gaus_ker0.end()); }
   vector<double>& get_MA_gaus_ker() { return MA_gaus_ker; }
   void set_ARMA_ker(vector<double> ARMA_ker0){ ARMA_ker.assign(ARMA_ker0.begin(),ARMA_ker0.end()); }
   vector<double>& get_ARMA_ker() { return ARMA_ker; }
 
  private:
    std::vector<double> MA_gaus_ker;
    std::vector<double> ARMA_ker;
    double fast_exp_ratio;
    double Afp_Aexp_ratio;
    double tau;
    double sigma_fp;
};

#endif

