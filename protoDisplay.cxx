#include <stdio.h>
#include <iostream>

#include "TRootanaDisplay.hxx"
#include "TH1D.h"
#include "TFancyHistogramCanvas.hxx"


#include "TAnaManager.hxx"
#include "TChronoData.h"

class MyTestLoop: public TRootanaDisplay { 

public:
	
  // An analysis manager.  Define and fill histograms in 
  // analysis manager.
  TAnaManager *anaManager;

  MyTestLoop() {
    SetOutputFilename("dsproto_display");
    DisableRootOutput(false);
    anaManager = new TAnaManager();
    // Number of events to skip before plotting one.
    SetNumberSkipEvent(1);
    // Choose to use functionality to update after X seconds
    //SetOnlineUpdatingBasedSeconds();
    // Uncomment this to enable the 'interesting event' functionality.
    //iem_t::instance()->Enable();
  }

  void AddAllCanvases(){

    // Set up tabbed canvases

    // Set up all the listed canvases from the AnaManager list of THistogramArrayBases
    std::vector<THistogramArrayBase*> histos = anaManager->GetHistograms();
    
    for (unsigned int i = 0; i < histos.size(); i++) {
      TCanvasHandleBase* canvas = histos[i]->CreateCanvas();
      if (canvas) {
        AddSingleCanvas(canvas, histos[i]->GetTabName());
      }
    }
        
    SetDisplayName("DS-Proto Display");
  };

  virtual ~MyTestLoop() {};

  void BeginRun(int transition,int run,int time) {
    anaManager->BeginRun(transition, run, time);
  }

  void EndRun(int transition,int run,int time) {
    anaManager->EndRun(transition, run, time);
  }

  void ResetHistograms(){}

  void UpdateHistograms(TDataContainer& dataContainer){
    // Update the cumulative histograms here
    anaManager->ProcessMidasEvent(dataContainer);
  }
  
  void PlotCanvas(TDataContainer& dataContainer){
    // Update the transient (per-event) histograms here.
    // saves CPU to not update them always when not being used.
    anaManager->UpdateTransientPlots(dataContainer);

  }
  
}; 






int main(int argc, char *argv[])
{
  MyTestLoop::CreateSingleton<MyTestLoop>();  
  return MyTestLoop::Get().ExecuteLoop(argc, argv);
}

